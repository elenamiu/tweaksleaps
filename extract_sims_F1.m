%%all similairities to previous entries
%%similarity in darkness
%%baseline comparison between contet 4 & 5 (blackbox & blockbuster)
%%similairities to current leader
%%similairities between leaders

simPATH = '~/Similarities/';
dataPATH ='~/Contest-Data/'
f1PATH='~/f1Data/'
simFiles = dir(strcat(simPATH, '*.mat'));
matFiles = dir(strcat(dataPATH,'*.mat'));
numfiles = length(matFiles);

leadsims = [];
llsims = [];
allsims=[];
for f=1:numfiles
    fName = simFiles(f).name;
    disp(fName)
    meh = load(strcat(simPATH,fName));
    czeka = meh.jSim2;
    
    fName = matFiles(f).name;
    disp(fName)
    meh = load(strcat(dataPATH,fName));
    d = meh.d;
    allLineList = meh.allLineList;
    
    %%first two days
    start_time = d(1).t;
    end_time = d(end).t;
    % dayBins = floor(start_time:1:end_time);
    dayBins = start_time:1:end_time;
    day = zeros(1,length(d));
    for j=1:(length(dayBins)-1)
        jday = find(datenum([d.t]) >= dayBins(j) & datenum([d.t]) < dayBins(j+1));
        day(jday)= j;
    end
    for j=1:length(d)
        d(j).day = day(j);
    end
    %%first entry in day 3
    firstDay3 = find([d.day] == 2,1,'last');
    
    %%leaders and daylight
    passedIndex = [d.passed]>0;
    dPassed = d([d.passed]>0);
    leadersIx = find([dPassed.initial_rank]==1);
    firstDay3 = find([dPassed.day] == 2,1,'last');
    leadersIx_day = leadersIx(leadersIx > firstDay3);
    czekaPassed = czeka(passedIndex, passedIndex);

    %% passed entries and daylight
    
    %extract lower triangular matrix without diagonal
    aux = tril(czekaPassed(firstDay3:end, firstDay3:end),-1);
    aux2 = aux(find(aux))';
    allsims = [allsims aux2];
    
    %sim to current leader
    %passed daylight
    lsims2 = zeros(1,length(dPassed) - firstDay3 +1);
    for j=firstDay3:length(dPassed)
        if j==1
            leader=1;
        else
            leader = find(leadersIx <j,1,'last');
        end
        lsims2(j-firstDay3+1) = czekaPassed(leadersIx(leader), j);
    end
    leadsims = [leadsims lsims2];
    
    %%leader to leader
    for lead = 2:length(leadersIx_day)
        llsims = [llsims czekaPassed(leadersIx_day(lead-1),leadersIx_day(lead))];
    end
end

%% write to file
% fname_path = strcat(dataPATH, 'allsims.mat')
% save(fname_path, 'allsims' )
% fname_path = strcat(dataPATH,'leadsims.mat')
% save(fname_path, 'leadsims' )
% fname_path = strcat(dataPATH, 'llsims.mat')
% save(fname_path, 'llsims' )


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% similarity within contest in darkness
%%%%%%%%%%%%%%%%

darkc = [1 4:9 13 14 16:18 20];
nodarkc = [3 10 11 12 15 19];
%only contests that have darkness
darksims = [];
darksimsdays = [];
for fi=1:length(darkc)
    f = darkc(fi);
    fName = simFiles(f).name;
    disp(fName)
    meh = load(strcat(simPATH,fName));
    czeka = meh.jSim2;
    
    fName = matFiles(f).name;
    disp(fName)
    meh = load(strcat(dataPATH,fName));
    d = meh.d;
    allLineList = meh.allLineList;
    
    %%leaders and daylight
    passedIndex = [d.passed]>0;
    dPassed = d([d.passed]>0);
    leadersIx = find([dPassed.initial_rank]==1);
    firstParent = find([dPassed.parent]>0,1);
    leadersIx_day = leadersIx(leadersIx > firstParent);    
    %leadersIx_day = [find([dPassed.initial_rank]==1)]>firstParent;
    czekaPassed = czeka(passedIndex, passedIndex);
    
    for j=2:firstParent
        darksims = [darksims czeka(j,1:j-1)];
    end
    
    %%first two days rather than firstparent
    start_time = d(1).t;
    end_time = d(end).t;
    % dayBins = floor(start_time:1:end_time);
    dayBins = start_time:1:end_time;
    day = zeros(1,length(d));
    for j=1:(length(dayBins)-1)
        jday = find(datenum([d.t]) >= dayBins(j) & datenum([d.t]) < dayBins(j+1));
        day(jday)= j;
    end
    for j=1:length(d)
        d(j).day = day(j);
    end
    %%yes do this!!
    firstDay3 = find([d.day] == 2,1,'last');
    for j=2:firstDay3
        darksimsdays = [darksimsdays czeka(j,1:j-1)];
    end
    
end

fname_path = strcat(dataPATH, 'darksims_day3.mat')
save(fname_path, 'darksimsdays' )


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%between two contests
%%%%%%%%%%%%%%%%%%%%%%%%%

f = 8;
fName = matFiles(f).name;
disp(fName)
meh = load(strcat(dataPATH,fName));
d1 = meh.d;
allLineList1 = meh.allLineList;

f = 9;
fName = matFiles(f).name;
disp(fName)
meh = load(strcat(dataPATH,fName));
d2 = meh.d;
allLineList2 = meh.allLineList;

allLineList_both = {};
nLines = 0;
%for first contest
for i=1:length(d1)
    d1(i).linesboth = zeros(size(d1(i).lines));
    for j=1:length(d1(i).lines)
        nLines = nLines + 1;
        allLineList_both{end+1} = allLineList1{d1(i).lines(j)};
        d1(i).linesboth(j) = nLines;
    end
end
%and second contest
for i=1:length(d2)
    d2(i).linesboth = zeros(size(d2(i).lines));
    for j=1:length(d2(i).lines)
        nLines = nLines + 1;
        allLineList_both{end+1} = allLineList1{d2(i).lines(j)};
        d2(i).linesboth(j) = nLines;
    end
end

[allLineList_bu,~,ix] = unique(allLineList_both);

for i = length(d1):-1:1
    d1(i).linesboth = ix(d1(i).linesboth);
end
for i = length(d2):-1:1
    d2(i).linesboth = ix(d2(i).linesboth);
end

%%and now similarity
%%matrix M(i,j) - frequency count - how many times line j appears in entry
%%i
for j=1:length(d1)
    d1(j).contest =8;
end
for j=1:length(d2)
    d2(j).contest =9;
end

%put both contests together
d = [d1 d2];

M=zeros(length(d),length(allLineList_bu));
for j=1:length(d)
    %frequencies - MM line values, counts4 - freq counts
    aux = d(j).linesboth';
    [MM, idx] = unique(sort(aux));
    counts4 = diff([0,idx']);
    M(j,MM) = counts4;
end

%%number of lines entry i and j have in common - including multiple
%%instances of the same line
nL = length(allLineList_bu);
freqShare = zeros(length(d));
for i=1:length(d)
    if int8(i / 100) == i / 100
        disp(i)
    end
    for j=1:i-1
       X = M(i,:);
       Y = M(j,:); 
       both = 0;
       for k=1:nL
           both = both + min(X(k),Y(k));
       end
       freqShare(i,j) = both;
    end
end
%
fs = freqShare(length(d1)+1:length(freqShare),1:length(d1));
fs2 = freqShare(1:length(d1),1:length(d1));
fs3 = freqShare(length(d1)+1:end,length(d1)+1:end);

% mean(mean(fs))
% ans =    8.0798
% 
% std(reshape(fs,[],1))
% ans = 15.5721

% mean(mean(fs3))
% ans =  172.2889

% std(reshape(fs3,[],1))
% ans = 356.0404

% mean(den) 
%  680.8922
 

% mean(reshape(fs2,[],1))
% ans =   71.3782

% std(reshape(fs2,[],1))
%   158.1565
   
% mean(den(length(d1)+1:end))
% ans =  948.2696

%%Chekanowski similarity
czeka = zeros(length(d));
den = arrayfun(@(x) length(x.lines), d);
for i=1:length(d)
%     disp(i)
    for j=1:i-1
        simVal = 2 .* freqShare(i,j) ./ (den(i)+den(j));
        %lower diagonal
        czeka(i,j) = simVal;
        %fill in the rest
        czeka(j,i) = simVal;
    end
end

%similarity between contest 8 and 9
sims = czeka(length(d1)+1:length(freqShare),1:length(d1));
%write to file
fname_path = strcat(f1PATH, 'sims89.mat')
save(fname_path, 'sims' )


        

