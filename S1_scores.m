dataPATH ='~/Contest-Data/'
matFiles = dir(strcat(dataPATH, '*.mat'));
numfiles = length(matFiles);

names = {'ants';'army ants';'binpack';'blackbox';'blockbuster';'color bridge';
    'crossword';'gene splicing';'gerrymandering'; 'mars surveyor'; 'mastermind';
    'molecule'; 'moving furniture'; 'peg solitaire'; 'protein folding';
    'sailing home'; 'sensor'; 'sudoku'; 'trucking freight'; 'wiring'}

%%%%%%%%%%%%%%%
ci = [1,3:20];
figure
for p = 1:19
    f = ci(p);
    subplot(5,4,p)
    aux = matFiles(f).name;
    disp(aux)
    meh = load(strcat(dataPATH,aux));
    d = meh.d;
    allLineList = meh.allLineList;
    dPassed = d([d.passed]>0);
    
    score = [dPassed.score];
    scores = score-min(score);
    times = [dPassed.t];
    best = Inf;
    %%add leaders to cell array
    bestIndex=1;
    for n=1:length(score),
        if (score(n) < best),
            best = score(n);
            bestIndex = [bestIndex n];
        end
    end
    leaders = bestIndex;
    %plot
    x = times;
    y=scores;
    %fix log of last leader
    y = log(scores+10);  
    plot(x,y,'ko','markersize',1)
    thisBestIndex = leaders;
    hold on
    stairs(x(thisBestIndex), y(thisBestIndex), 'r.-','MarkerSize',8)
    set(gca, 'YScale','log')
    set(gca,'xlim',[min(x) max(x)])
    set(gca, 'ylim', [0 max(y)])
    set(gca, 'xtick',[])
    set(gca, 'xticklabel',[])
%     datetick('keeplimits')
    fName = matFiles(f).name
    title(names(f));    
end

set(gcf, 'PaperUnits', 'inches','PaperPosition', [.25 2.5 6.85 8])

picName = 'scores_pub.pdf'
print('-dpdf','-r600',picName)
% 
picName = 'scores.tiff'
print('-dtiff','-r300',picName)






