dataPATH ='~/Contest-Data/';
simPATH = '~/Similarities/';
matFiles = dir(strcat(PATH, '*.mat');
numfiles = length(matFiles);

for f=1:numfiles
    fName = matFiles(f).name;
    disp(fName)
    meh = load(strcat(dataPATH,fName));
    d = meh.d;
    allLineList = meh.allLineList;
    %%matrix M(i,j) - frequency count - how many times line j appears in entry
    %%i
    M=zeros(length(d),length(allLineList));
    for j=1:length(d)
        %frequencies - MM line values, counts4 - freq counts
        %aux = d(j).lines';
        aux=d(j).lines;
        [MM, idx] = unique( sort(aux) );
        %counts4 = diff([0,idx]);
        counts4=diff([0;idx]);
        M(j,MM) = counts4;
    end

    %%number of lines entry i and j have in common - including multiple
    %%instances of the same line   
    freqShare = zeros(length(d));
    nL = length(allLineList);
    for i=1:length(d)
        if int8(i / 100) == i / 100
            disp(i)
        end
        for j=1:i-1
           X = M(i,:);
           Y = M(j,:); 
           both = 0;
           for k=1:nL
               both = both + min(X(k),Y(k));
           end
           freqShare(i,j) = both;
        end
    end

    %%Chekanowski similarity

    jSim = zeros(length(d));
    den = arrayfun(@(x) length(x.lines), d);
    for i=1:length(d)
        disp(i)
        for j=1:i-1
            simVal = 2 .* freqShare(i,j) ./ (den(i)+den(j));
            jSim(i,j) = simVal;
        end
    end
    %fill in the other triangle
    jSim2 = jSim;
    for i=1:size(jSim,2)
        for j=1:i-1
            jSim2(j,i) = jSim2(i,j);
        jSim2(i,i) = 1;
        end
    end
    
    simFile = strcat(simPATH,fName);
    save(simFile, 'jSim2');
end
