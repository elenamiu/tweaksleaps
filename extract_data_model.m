%%
%%REDO
dataPATH = '~/Contest-Data/';
modelPATH='~/Model-Data/';
matFiles = dir(strcat(dataPATH, '*.mat'));
numfiles = length(matFiles);

diversityAll = cell(1,20);
prop_diversityAll = cell(1,20);
for f=1:numfiles
    aux = matFiles(f).name;
    disp(aux)
    meh = load(strcat(dataPATH,aux));
    d = meh.d;
    allLineList = meh.allLineList;  
    contest = aux(1:end-4);
    
    passedIndex = [d.passed]>0;
    dPassed = d(passedIndex);
    %%
    A = [];
    uA=[];
    linesSoFar=cell(1,length(dPassed));
    for j=1:length(dPassed)
        if int8(j/300) == j/300
           disp(j)
        end
        A = [A d(j).lines];
        uA = [uA d(j).lines];
        uA = unique(uA);
        linesSoFar{j} = A;
    end
    
    window = ceil(length(dPassed) ./ 100);
    prop_diversity = [];
    time_diff = [];
    time = [];
    time_sc = [];
    noPlayers = [];
    
    for j=window:window:length(dPassed)
        lines_r = unique(linesSoFar{j});
        lines_l = unique(linesSoFar{j-window+1}); 
        uLines = setdiff(lines_r, lines_l);
        prop_diversity = [prop_diversity length(uLines) ./ length(allLineList)];
        time_diff = [time_diff dPassed(j).t - dPassed(j-window+1).t];
        time = [time dPassed(j).t];
        time_sc = [time_sc dPassed(j).t - dPassed(1).t];
        noPlayers = [noPlayers length(unique([dPassed(j-window+1:j).author_id]))];
    end    
    
        
    div = zeros(3,length(time));
    div(1,:) = prop_diversity;
    div(2,:) = time;
    div(3,:) = noPlayers;
    div = div';

    fname = matFiles(f).name;
    fname_path = strcat(modelPATH,fname(1:length(fname)-4),'_div.mat')
    save(fname_path, 'div' )
end