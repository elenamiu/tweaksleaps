%%%
%uses similarity values calculated with calculate_sims.m
simPATH = '~/Similarities/';
dataPATH ='~/Contest-Data/'
edPATH='~/Extracted-Data/'
simFiles = dir(strcat(simPATH, '*.mat'));
matFiles = dir(strcat(dataPATH,'*.mat'));
numfiles = length(matFiles);

for f=[1 3:numfiles]
    %similarity
    fName = simFiles(f).name;
    disp(fName)
    meh = load(strcat(simPATH,fName));
    czeka = meh.jSim2;
    %contest data
    fName = matFiles(f).name;
    disp(fName)
    meh = load(strcat(dataPATH,fName));
    d = meh.d;
    allLineList = meh.allLineList;
    
    %% remove outlier
    if (f == 17)
        out = 217;
        d(out) = [];        
    end
    %split by days
    dates = [d.t];
    dayBins = floor(dates(1)):1:ceil(dates(end));
    
    %day index
    day = zeros(1,length(d));
    for j=1:(length(dayBins)-1)
        jday = find([d.t] >= dayBins(j) & [d.t] < dayBins(j+1));
        day(jday) = j;
    end
        
    %add original index to the data structure
    %and day
    for j=1:length(d)
        d(j).ix = j;
        d(j).day = day(j);
    end

    %bit of tidying up
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% INITIAL AUTHOR 
    % track each line back to its initial author
    % assign an author to each line
    % -- all entries
    authors = zeros(1, length(allLineList));
    for j=1:length(d)
        %find lines in this entry that don't have an author yet
        thisLines = d(j).lines;
        noAuthor = find(authors(thisLines)==0);
        %assing current author to those lines
        authors(thisLines(noAuthor))=j;
    end   
    %
    %for each entry, a list of the original authors of the lines in the, in a matrix
    % oP(i,j)=n, how many lines in i belong to j
    % opP(i,j) = pn - proportion of lines in i belonging to j
    % opP(entry, source)
    oP = zeros(length(d));
    opP = zeros(length(d));
    for j=1:length(d)
        thisLines = d(j).lines;
        den = length(d(j).lines) - length(d(j).new_lines);
        parents = authors(thisLines);
        uqParents = unique(parents);
        counts = histc(parents, uqParents);
        oP(j, uqParents)=counts;
        if den > 0
            opP(j, uqParents) = counts / den;
        else
            opP(j,uqParents) = 0;
        end
    end 
         
    %%     
    %medians of the "parent" distribution for each entry
    % and max value
    % -- all entries entries
    nparents = zeros(1,length(d));
    for j=1:length(d)
        %grab the original authors without self    
        ps = opP(j,1:j-1);
        parents = ps(ps>0);
        % if only borrowed from one other entry, not recombination
        if length(parents) == 1
            nparents(j)=1;  
        % if they've borrowed from at least two other entries
        elseif sum(ps>0)
            nparents(j) = length(parents);
        % if they haven't borrowed, not recombination
        else
            nparents(j)=0;
        end
        d(j).nparents = nparents(j);
    end 
    
    %%
    % MOST RECENT AUTHOR
    % track each line back to its most recent author
    % boolean matrix - Mel(i,j) = 1 if entry i contains line j
    % Mel(entry, line)
    Mel = zeros(length(d),length(allLineList));
    for j=1:length(d)
        thisLines = d(j).lines;
        Mel(j,thisLines) = 1;
    end    
    
    % Matrix rpP(i,j) = how many lines in i have j as its most recent
    % source
    % rpP(entry, source) 
    % expressed as proportion of the number of borrowed lines in i
    rpP = zeros(length(d));
    for j=1:length(d)
        thisLines = d(j).lines;
        borrowed = setdiff(thisLines, d(j).new_lines);
        if borrowed
            recent_source = zeros(1,length(borrowed));
            for k=1:length(borrowed)
                line = borrowed(k);
                %find all entries that contain line 
                sources = find(Mel(:,line));
                recent_source(k) = sources(find(sources<j, 1,'last'));
            end
            uqParents = unique(recent_source);
            counts = histc(recent_source, uqParents);
            rpP(j, uqParents) = counts / length(borrowed);
%         else            
        end
    end
       

    rnparents = zeros(1,length(d));
    for j=1:length(d)
        %grab the original authors without self    
        %because authors were calculated before we subsetted the data and
        %only kept passed entries, we want to look at the original parents
        %i.e. before d(j).ix(the index in the original d), not before j
        rps = rpP(j,1:d(j).ix-1);
        rparents = rps(rps>0);
        pars{j} = rparents;
        % if only borrowed from one other entry, not recombination
        if length(rparents) == 1
            rnparents(j)=1;  
        % if they've borrowed from at least two other entries
        elseif sum(rps>0)
            rnparents(j) = length(rparents);
        % if they haven't borrowed, not recombination
        else
            rnparents(j)=0;
        end
        d(j).rnparents = rnparents(j);
    end 
    %%
    %passed entries index
    passedIndex = find([d.passed]>0);
    dPassed = d(passedIndex);
    czekaPassed = czeka(passedIndex, passedIndex);
    % from here on 
    % just passed
    %leaders index
    leadersIx = find([dPassed.initial_rank]==1);
    % similarity to current leader
    % and increment
    lsims = zeros(1,length(dPassed));
    increments = zeros(1,length(dPassed));
    %entry to leader
    for j=1:length(dPassed)
        if j==1
            leader=1;
        else
            leader = find(leadersIx < j,1,'last');
        end
        lsims(j) = czekaPassed(leadersIx(leader), j);
        dPassed(j).lsim = lsims(j);
        % and increments
        % positive for new leaders, negative for the rest
        increments(j) = dPassed(leadersIx(leader)).score - dPassed(j).score;
        dPassed(j).increment = increments(j);
    end   
    %% add length of entry to the structure
    for j=1:length(dPassed)
        dPassed(j).len = length(dPassed(j).lines);
    end
    %% new lines per entry
    for (j=1:length(dPassed))
        dPassed(j).new = length(dPassed(j).new_lines);
        dPassed(j).prop_new = length(dPassed(j).new_lines) / length(dPassed(j).lines);
    end

    alrec = zeros(11,length(dPassed));
    alrec(1,:) = [dPassed.nparents];
    alrec(2,:) = [dPassed.rnparents];
    alrec(3,:) = [dPassed.lsim];
    alrec(4,:) = [dPassed.increment];
    alrec(5,:) = [dPassed.new];
    alrec(6,:) = [dPassed.prop_new];
    alrec(7,:) = [dPassed.len];
    alrec(8,:) = [dPassed.initial_rank] == 1;
    alrec(9,:) = [dPassed.passed] == 1;
    alrec(10,:) = [dPassed.day];
    alrec(11,:) = [dPassed.t];
    alrec = alrec';
    
    fname = matFiles(f).name;
    fname_path = strcat(edPATH,fname(1:length(fname)-4),'.mat')
    save(fname_path, 'alrec' )
end