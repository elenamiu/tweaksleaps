%% Figure 2
simPATH = '~/Similarities/';
dataPATH ='~/Contest-Data/'
simFiles = dir(strcat(simPATH, '*.mat'));
matFiles = dir(strcat(dataPATH,'*.mat'));
numfiles = length(matFiles);


poza = figure; 
fs = [9 14 6 10];
labs = char('A','B','C','D');
for j=[1:4]
    f=fs(j);
    fName = simFiles(f).name;
    disp(fName)
    meh = load(strcat(simPATH,fName));
    czeka = meh.jSim2;
    
    fName = matFiles(f).name;
    disp(fName)
    meh = load(strcat(dataPATH,fName));
    d = meh.d;
    allLineList = meh.allLineList;
    
    %%heatmap of leaders
    subplot(2,2,j)
    %only children
    childrenIx = find([d.parent]>0);
    colormap('hot');
    imagesc(czeka(childrenIx,childrenIx))
    label = labs(j,:);
    if j == 1 
        text(-220, -120, label, 'FontSize', 14)
    elseif j == 2 
        text(-320, -140, label, 'FontSize', 14)
    elseif j == 3
        text(-220, -120, label, 'FontSize', 14)
    else 
        text(-200, -100, label, 'FontSize', 14)
    end
    set(gca, 'XAxisLocation', 'top')
    set(gca, 'fontsize', 8)
end

c = colorbar;
c.Position = [0.93 0.12 0.04 0.8]
set(gcf, 'units', 'centimeters')
size = get(gcf, 'Position');
set(gcf, 'pos', [size(1) size(2) 12.7 12.7])
size = size(3:4);
set(gcf, 'PaperSize', size)


picFile = strcat(dataPATH,'heatmaps')
set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', [0 0 12 12])

% print(poza,'-dpdf', '-r600',picFile);
print(poza,'-dtiff', '-r300', picFile);
 close(poza)